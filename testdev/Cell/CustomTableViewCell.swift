//
//  CustomTableViewCell.swift
//  testdev
//
//  Created by Didier HOUSSIN on 10/01/2021.
//  Copyright © 2021 Gira. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var townImage: UIImageView!
    @IBOutlet weak var townName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
